# Petstore Test ASSIGNMENT

Author: Giovani Rodrigues da Silva

To run the test cases you will need:

Java 8
Serenity-bdd plugin (Should be already on the pom.xml)

What was done:
1 - Tested the basic requests of Pet endpoint / 1 negative scenario
2 - Some requests of Store endpoint

To write new test case you need to follow the below instruction:
1- Create a new feature file if necessary or add your scenario in a feature file already created
Path: src>test>resources>features>status

2- Write the step definitions that are related to the feature files
Path: src>test>java>starter>stepdefinitions

3- Reuse as much as possible step definitions to avoid duplicated or similar code in different methods

To run the tests cases you can run basically using the gradlew command or selecting the desired
feature to be run
E.g for gradlew :  gradlew clean test
For quickly check on the feature file use Shift+F10


To see the reports of the test case:
Run the "gradlew clean test" and look at target/site/serenity directory. You should find an index.html file, which will be the home page of the Serenity Reports