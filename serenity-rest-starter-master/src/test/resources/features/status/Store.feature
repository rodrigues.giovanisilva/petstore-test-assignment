Feature: Store requests

  Scenario: Place an order for a pet
    Given I have an order to add
    When I place an order
    Then The order should be created

  Scenario: Deleting an order
    Given I have an order to delete
    When I delete an order
    Then The order should be deleted