Feature: Pet requests

  Scenario: Adding pet
    Given I have a pet to add
    When I add a pet
    Then The pet should be added

  Scenario: Deleting pet
    Given I have a pet to delete
    When I delete a pet
    Then The pet should be deleted

  Scenario: Updating pet
    Given I have a pet to update
    When I update a pet
    Then The pet should be updated

    #Negative scenario
  Scenario: Finding a non-existent pet
    Then The pet should be not found
