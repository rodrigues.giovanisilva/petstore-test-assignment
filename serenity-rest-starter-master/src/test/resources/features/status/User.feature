Feature: User requests

  Scenario: Create an user
    Given I have an user to create
    When I add a new user
    Then The user should be added

  Scenario: Delete an user
    Given I have an user to delete
    When I delete an user
    Then The user should be deleted