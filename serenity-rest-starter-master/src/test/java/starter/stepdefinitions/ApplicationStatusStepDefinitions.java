package starter.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.Random;
import static net.serenitybdd.rest.SerenityRest.rest;
import static org.hamcrest.Matchers.*;

public class ApplicationStatusStepDefinitions {

    Pet pet;
    int id = Math.abs(new Random().nextInt());

    String jsonPet = "{\"id\": " + id + " , \"name\": \""
            + "Test" + "\", \"photoUrls\": [], \"status\": \""
            + "Available" + "\"}";

    String jsonPet2 = "{\"id\": " + id + " , \"name\": \""
            + "TestUpdated" + "\", \"photoUrls\": [], \"status\": \""
            + "Available" + "\"}";

    String jsonOrder = "{\"id\": " + id + " ,  \"petId\": " + id + " ,  \"quantity\": 20, \"shipDate\": \"2020-08-22T15:52:51.144+0000\",  \"status\": \"placed\",  \"complete\": true}";

    String jsonUser = "{  \"id\": " + id + ",  \"username\": " + id +",  \"firstName\": \"string\",  \"lastName\": \"string\",  \"email\": \"string\",  \"password\": \"string\",  \"phone\": \"string\",  \"userStatus\": 0}";

    @Given("I have a pet to add")
    public void iHaveAPetToAdd() {

    }

    @When("I add a pet")
    public void iAddAPet() {
        System.out.println("jsonPet:" +jsonPet);
        rest().given().contentType("application/json")
                .content(jsonPet).post("https://petstore.swagger.io/v2/pet").then().statusCode(200);
        //this.pet.setId(id);
    }

    @Then("The pet should be added")
    public void thePetShouldBeAdded() {
        rest().get("https://petstore.swagger.io/v2/pet/{id}", id)
                .then().statusCode(200)
                .and().body("name", equalTo("Test"));
    }

    @Given("I have a pet to delete")
    public void iHaveAPetToDelete() {
        iAddAPet();
    }

    @When("I delete a pet")
    public void iDeleteAPet() {
        rest().delete("https://petstore.swagger.io/v2/pet/{id}", id)
                .then().statusCode(200);
    }

    @Then("The pet should be deleted")
    public void thePetShouldBeDeleted() {
        rest().get("https://petstore.swagger.io/v2/pet/{id}", id)
                .then().statusCode(404);
    }


    @Given("I have a pet to update")
    public void iHaveAPetToUpdate() {
        iAddAPet();
    }

    @When("I update a pet")
    public void iUpdateAPet() {
        rest().given().contentType("application/json")
                .content(jsonPet2).put("https://petstore.swagger.io/v2/pet").then().statusCode(200);
    }

    @Then("The pet should be updated")
    public void thePetShouldBeUpdated() {
        rest().get("https://petstore.swagger.io/v2/pet/{id}", id)
                .then().statusCode(200).and().body("name", equalTo("TestUpdated"));
    }

    @Then("The pet should be not found")
    public void thePetShouldBeNotUpdated() {
        rest().get("https://petstore.swagger.io/v2/pet/{id}", id)
                .then().statusCode(404);
    }


    @Given("I have an order to add")
    public void iHaveAnOrderToAdd() {

    }

    @When("I place an order")
    public void iPlaceAnOrder() {
        System.out.println("jsonOrder:" +jsonOrder);
        rest().given().contentType("application/json")
                .content(jsonOrder).post("https://petstore.swagger.io/v2/store/order").then().statusCode(200);
        //this.pet.setId(id);
    }

    @Then("The order should be created")
    public void theOrderShouldBeCreated() {
        rest().get("https://petstore.swagger.io/v2/store/order/{id}", id)
                .then().statusCode(200)
                .and().body("petId", equalTo(id));
    }

    @Given("I have an order to delete")
    public void iHaveAnOrderToDelete() {
        iPlaceAnOrder();
    }

    @When("I delete an order")
    public void iDeleteAnOrder() {
        rest().delete("https://petstore.swagger.io/v2/store/order/{id}", id)
                .then().statusCode(200);
    }

    @Then("The order should be deleted")
    public void theOrderShouldBeDeleted() {
        rest().get("https://petstore.swagger.io/v2/store/order/{id}", id)
                .then().statusCode(404);
    }


    @Given("I have an user to create")
    public void iHaveAnUserToCreate() {

    }

    @When("I add a new user")
    public void iAddANewUser() {
        System.out.println("jsonUser:" +jsonUser);
        rest().given().contentType("application/json")
                .content(jsonUser).post("https://petstore.swagger.io/v2/user").then().statusCode(200);
    }

    @Then("The user should be added")
    public void theUserShouldBeAdded() {
        String idd = Integer.toString(id);
        System.out.println("TESTE");
        rest().get("https://petstore.swagger.io/v2/user/{username}",id)
                .then().statusCode(200).and().body("username", equalTo(idd));
    }



    @Given("I have an user to delete")
    public void iHaveAnUserToDelete() {
        iAddANewUser();
    }

    @When("I delete an user")
    public void iDeleteAnUser() {
        System.out.println("jsonUser:" +jsonUser);
        rest().delete("https://petstore.swagger.io/v2/user/{username}", id)
                .then().statusCode(200);
    }

    @Then("The user should be deleted")
    public void theUserShouldBeDeleted() {
        String idd = Integer.toString(id);
        System.out.println("TESTE");
        rest().get("https://petstore.swagger.io/v2/user/{username}",id)
                .then().statusCode(404);
    }

}
